import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text, ScrollView, FlatList } from 'react-native';
import { Header, FormLabel, FormInput, Icon, Badge } from "react-native-elements";

const HANDLER_HEIGHT = 30;
const { width } = Dimensions.get("window");

export default class Handler extends Component {

    constructor(props){
        super(props)
        this.state = {
            data: props.data,
            icon: 'expand-more'
        };
    }

    setData(data){
        this.setState({ data: data, icon: this.state.icon });
    }
    
    render(){
        return (
        <View style={styles.handler}>
            <View style={ styles.selectionContainer }>
                <FlatList horizontal={ true } data={ this.state.data } style={ {flex: 1} }
                    renderItem={({item}) =>{ 
                        return(
                            <Badge containerStyle={{ flex: 0, backgroundColor: '#009688', padding: 10, margin: 10 }}>
                                <Text style={{ color: '#fff'}}>{item.label}</Text>
                            </Badge>
                        );      
                    }} />

            </View>
            <View style={ styles.buttonContainer }>
                <Icon name={this.state.icon} color="black" onPress={ () => {
                        console.log(this.state.icon);
                        if (this.state.icon == 'expand-more'){
                            ico = 'expand-less';
                        } else {
                            ico = 'expand-more';
                        }
                        this.state.icon = ico;
                        this.setState({data: this.state.data, icon: ico});
                        this.props.onButtonClick(); 
                    }} />
            </View>
        </View>
        )
    };
  }

  const styles = StyleSheet.create({
    backContainer: {
      marginTop: HANDLER_HEIGHT,
      backgroundColor: "green"
    },
    
    frontContainer: {
      flex: 1,
      width: width,
      backgroundColor: "papayawhip"
    },

    handler: {
        flexDirection: 'row',
        width: width,
        borderBottomColor: '#80808080',
        borderBottomWidth: 1
        
    },

    selectionContainer: {
        flex:8,
        flexDirection: 'row',
        padding: 10,
    },
    buttonContainer: {
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }

  });