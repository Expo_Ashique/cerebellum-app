import React, { Component } from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { ParallaxImage } from 'react-native-snap-carousel';
import styles from './style';
import { Thumbnail, Left, CardItem, Right } from 'native-base';
import { Button } from 'react-native-vector-icons/Feather';
import Icon  from 'react-native-vector-icons/FontAwesome';
import CheckBox from 'react-native-check-box'

export default class JournalItem extends Component {

    static propTypes = {
        data: PropTypes.object.isRequired,
        even: PropTypes.bool,
        parallax: PropTypes.bool,
        parallaxProps: PropTypes.object
    };

    get image () {
        const { data: { uri }, parallax, parallaxProps, even, publishTime } = this.props;

        return parallax ? (
            <ParallaxImage
              source={{ uri: uri }}
              containerStyle={[styles.imageContainer, even ? styles.imageContainerEven : {}]}
              style={styles.image}
              parallaxFactor={0.35}
              showSpinner={true}
              spinnerColor={even ? 'rgba(255, 255, 255, 0.4)' : 'rgba(0, 0, 0, 0.25)'}
              {...parallaxProps}
            />
        ) : (
            <Image
              source={{ uri: uri }}
              style={styles.image}
            />
        );
    }

    render () {
        const { data: { title, subtitle, uri, author, publishTime, summary }, even } = this.props;

        const uppercaseTitle = title ? (
            <Text
              style={[styles.title, even ? styles.titleEven : {}]}
              numberOfLines={2}
            >
                { title.toUpperCase() }
            </Text>
        ) : false;

        return (
            <TouchableOpacity
              activeOpacity={1}
              style={styles.slideInnerContainer}
              onPress={() => {  }}>
                <View style={styles.shadow} />
                <View style={{ flex: 1, backgroundColor: 'white'}}>
                    <CardItem>
                            <Left>
                                {/* <Thumbnail source={{uri: uri}} /> */}
                                <View style={{ flexDirection: 'column'}}>
                                    <Text style={ styles.title } numberOfLines={ 3 }> { author } </Text>
                                    <Text style={ styles.publishTime }>{ publishTime }</Text>
                                </View>
                            </Left>
                            
                            
                        
                    </CardItem>
                    
                    <CardItem style={{ flexDirection: 'column'}}>
                        <Text>{ summary.substring(0, Math.min(400, summary.length)) }</Text>
                        <TouchableOpacity onPress={ () => this.props.onItemClick(this.props.data) }>
                            <Text style={{color:"#5fcf80", fontWeight: 'bold'}}>Read More</Text>
                        </TouchableOpacity>
                    </CardItem>
                    <CardItem style={{ alignItems: 'flex-end' }}>
                    
                        </CardItem>

                </View>
                <View style={[styles.textContainer, even ? styles.textContainerEven : {}, { flexDirection: 'row', marginBottom: 20,}]}>
                    <View style={{ flexDirection: 'column', flex: 5}}>
                        { uppercaseTitle }
                        <Text
                        style={[styles.subtitle, even ? styles.subtitleEven : {}]}
                        numberOfLines={2}>
                            { subtitle }
                        </Text>
                    </View>
                    <View style={{ flex: 1, alignItems: 'stretch', justifyContent: 'center'}}>
                        <CheckBox
                                style={{flex: 1, padding: 10}}
                                onClick={ () => {}}
                                isChecked={true}
                                checkedImage={<Icon name="bookmark" color="#5fcf80" size={ 24 }/>}
                                unCheckedImage={<Icon name="bookmark-o" color="#5fcf80" size={ 24 }/>}
            
                            />
                            </View>
                        

                    
                </View>
            </TouchableOpacity>
        );
    }
}
