import React, { Component } from 'react';
import { TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const styles = StyleSheet.create({

    TouchableOpacityStyle:{   
        backgroundColor: '#ff3366',
        position: 'absolute',
        width: 60,
        height: 60,
        alignItems: 'center',
        justifyContent: 'center',
         borderRadius: 30,
        shadowColor: 'black',
        elevation: 8
    },
   
    FloatingButtonStyle: {
      
        
      
    }
});

export default class Fab extends Component {
    render(){
        return (
            <TouchableOpacity activeOpacity={0.5} onPress={this.SampleFunction} style= { [styles.TouchableOpacityStyle, this.props.style ]} >
                <Icon name={ this.props.icon } size={ 30 } color='white' />

            </TouchableOpacity>
        );
    }
}