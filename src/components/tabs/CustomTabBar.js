import React from 'react';
import { TabBar, TouchableItem } from 'react-native-tab-view';


import {
    Animated,
    NativeModules,
    StyleSheet,
    View,
    ScrollView,
    Platform,
    I18nManager,
    TouchableOpacity
  } from 'react-native';


const useNativeDriver = Boolean(NativeModules.NativeAnimatedModule);

  
export default class CustomTabBar extends TabBar {

    _renderLabel = (scene) => {
        const label = this.props.getLabelText(scene);
        if (typeof label !== 'string') {
          return null;
        }
        return (
          <Animated.Text style={[styles.tabLabel, this.props.labelStyle]}>
            {label}
          </Animated.Text>
        );
      };
    

      _getTabWidth = (props) => {
          return 100;
      }

    //   _renderIndicator = (props) => {
    //     console.log(props.navigationState.index);
    //     console.log(props);
    //   };

    render() {
        const { position, navigationState, scrollEnabled, bounces } = this.props;
        const { routes, index } = navigationState;
        const tabWidth = this._getTabWidth(this.props);
        const tabBarWidth = tabWidth * routes.length;

        // Prepend '-1', so there are always at least 2 items in inputRange
        const inputRange = [-1, ...routes.map((x, i) => i)];
        const translateX = Animated.multiply(this.state.scrollAmount, -1);

        return (
        <Animated.View style={[styles.tabBar, this.props.style]}>
            
            <Animated.View
                pointerEvents="none"
                style={[
                    styles.indicatorContainer,
                    scrollEnabled
                    ? { width: tabBarWidth, transform: [{ translateX }] }
                    : null,
                ]}>
                {/* {this._renderIndicator({
                    ...this.props,
                    width: tabWidth,
                })} */}
            </Animated.View>

            <View style={styles.scroll}>
                <Animated.ScrollView
                    horizontal
                    keyboardShouldPersistTaps="handled"
                    scrollEnabled={scrollEnabled}
                    bounces={bounces}
                    alwaysBounceHorizontal={false}
                    scrollsToTop={false}
                    showsHorizontalScrollIndicator={false}
                    automaticallyAdjustContentInsets={false}
                    overScrollMode="never"
                    contentContainerStyle={[
                    styles.tabContent,
                    scrollEnabled ? null : styles.container,
                    ]}
                    scrollEventThrottle={1}
                    onScroll={Animated.event(
                    [
                        {
                        nativeEvent: {
                            contentOffset: { x: this.state.scrollAmount },
                        },
                        },
                    ],
                    { useNativeDriver }
                    )}
                    onScrollBeginDrag={this._handleBeginDrag}
                    onScrollEndDrag={this._handleEndDrag}
                    onMomentumScrollBegin={this._handleMomentumScrollBegin}
                    onMomentumScrollEnd={this._handleMomentumScrollEnd}
                    contentOffset={this.state.initialOffset}
                    ref={this._setRef}>
                {routes.map((route, i) => {
                    const focused = index === i;
                    const outputRange = inputRange.map(
                        inputIndex => (inputIndex === i ? 1 : 0.7)
                    );
                    const opacity = Animated.multiply(
                        this.state.visibility,
                        position.interpolate({
                        inputRange,
                        outputRange,
                        })
                    );
                    const scene = {
                        route,
                        focused,
                        index: i,
                    };
                    const label = this._renderLabel(scene);
                    const icon = this.props.renderIcon
                        ? this.props.renderIcon(scene)
                        : null;
                    const badge = this.props.renderBadge
                        ? this.props.renderBadge(scene)
                        : null;

                    const tabStyle = {};

                    tabStyle.opacity = opacity;

                // if (icon) {
                //     if (label) {
                //     tabStyle.paddingTop = 8;
                //     } else {
                //     tabStyle.padding = 4;
                //     }
                // }

                    const passedTabStyle = StyleSheet.flatten(this.props.tabStyle);
                    const isWidthSet =
                        (passedTabStyle &&
                        typeof passedTabStyle.width !== 'undefined') ||
                        scrollEnabled === true;
                    let tabContainerStyle = {};

                    // if (isWidthSet) {
                    //     tabStyle.width = tabWidth;
                    // }

                    if (passedTabStyle && typeof passedTabStyle.flex === 'number') {
                        tabContainerStyle.flex = passedTabStyle.flex;
                    } else if (!isWidthSet) {
                        tabContainerStyle.flex = 0;
                    }

                    
                    
                    tabContainerStyle = {};

                    const accessibilityLabel =
                        route.accessibilityLabel || route.title;

                    return (
                        <TouchableOpacity
                        borderless
                        key={route.key}
                        testID={route.testID}
                        accessible={route.accessible}
                        accessibilityLabel={accessibilityLabel}
                        accessibilityTraits="button"
                        pressColor={this.props.pressColor}
                        pressOpacity={this.props.pressOpacity}
                        delayPressIn={0}
                        onPress={() => this._handleTabPress(scene)}
                        style={tabContainerStyle}
                        >
                        <View pointerEvents="none" style={styles.container}>
                            <Animated.View
                            style={[
                                styles.tabItem,
                                tabStyle,
                                passedTabStyle,
                                styles.container
                            ]}
                            >
                            {icon}
                            {label}
                            </Animated.View>
                            {badge ? (
                            <Animated.View
                                style={[
                                styles.badge,
                                { opacity: this.state.visibility },
                                ]}
                            >
                                {badge}
                            </Animated.View>
                            ) : null}
                        </View>
                        </TouchableOpacity>
                    );
                    })}
                </Animated.ScrollView>
            </View>
        </Animated.View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
      flex: 0,
    },
    scroll: {
      overflow: Platform.OS === 'web' ? 'auto' : 'scroll',
    },
    tabBar: {
      backgroundColor: '#2196f3',
      elevation: 0,
      shadowColor: 'black',
      shadowOpacity: 0.1,
      shadowRadius: StyleSheet.hairlineWidth,
      shadowOffset: {
        height: StyleSheet.hairlineWidth,
      },
      // We don't need zIndex on Android, disable it since it's buggy
      zIndex: Platform.OS === 'android' ? 0 : 1,
    },
    tabContent: {
      flexDirection: 'row',
      flexWrap: 'nowrap',
    },
    tabLabel: {
      backgroundColor: 'transparent',
      color: 'white',
      marginVertical: 8,
      marginHorizontal: 8,
    },
    tabItem: {
      flex: 0,
      padding: 8,
      alignItems: 'center',
      justifyContent: 'center',
    },
    badge: {
      position: 'absolute',
      top: 0,
      right: 0,
    },
    indicatorContainer: {
      position: 'absolute',
      top: 0,
      left: 0,
      right: 0,
      bottom: 0,
    },
    indicator: {
      backgroundColor: '#ffeb3b',
      position: 'absolute',
      left: 0,
      bottom: 0,
      right: 0,
      height: 2,
    },
  });