import React, { Component } from 'react';
import { FlatList } from 'react-native';
import SearchResultListItem from './SearchResultListItem'

export default class SearchResultList extends Component {
    render(){
        return(
            <FlatList
                data={ this.props.articles }
                renderItem={ ( { item } ) => <SearchResultListItem article = { item } /> }
            />

            
        );
    }
}