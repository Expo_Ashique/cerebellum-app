import React, { Component } from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import CheckBox from 'react-native-check-box'
import Icon  from 'react-native-vector-icons/FontAwesome';
import { Row, Divider, TouchableOpacity } from '@shoutem/ui'


const styles = {
    container: {
        flexDirection: 'row',
        padding: 15
    },

    textContainer: {
        justifyContent: 'center',
        marginRight: 20
    },

    checkBoxContainer:{
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },

    bookmarkCheckBox: {
        padding: 10
    },

    category: {
        fontSize: 18,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 10,
    },

    time: {
        fontSize: 14,
        marginBottom: 10
    },

    title: {
        fontSize: 20,
        color: 'black',
        textAlign:'left',
        marginBottom: 10,
        paddingRight: 30,
    },

    image:{
        width: 50,
        height: 74,
        resizeMode: 'cover',
        marginRight: 10
    },

    

}

export default class SearchResultListItem extends Component {
    render(){
        const { article } = this.props;
        return(
            <TouchableOpacity>
                <View style={ styles.container }>
                    <Image source={ require('../../../images/journal.jpg') } style={ styles.image } />
                    <View style={ styles.textContainer }>
                        <Text style={ styles.title } noOfLines={ 2 } > { article.title } </Text>
                    </View>
                </View>
                <Divider styleName="line" />
            </TouchableOpacity>
        );
    }
}