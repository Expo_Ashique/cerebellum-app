import React , {Component} from "react";
import {TouchableOpacity,Button} from 'react-native';
import  Ionicons from 'react-native-vector-icons/Ionicons';
import {Body,Text,Thumbnail,View,Content,Container,List,ListItem,Left,Right,Item} from 'native-base';

import Time from './Time';


export default class ListDataItem extends Component{
    constructor(props){
        super(props);
        this.data = props.data
    }

    render(){
        return (
            <TouchableOpacity style={{flexDirection:'row'}} activeOpacity={0.5} onPress={() => this.props.navigation.navigate('Details',{new_data:this.data})}>

                <Body>
                    <Text style={{fontSize:16}} numberOfLines={1}>{this.data.title}</Text>
                    <Time time={ this.data.publishedAt}/>
                    <Text note numberOfLines={2}>{this.data.description}</Text>
                    <View style={{flex:1,flexDirection:'row',marginTop:8,marginLeft:8}}>
                        <Text note>{this.data.source.name}</Text>
                    </View>
                </Body>
                <Right>
                    <Ionicons name="ios-settings" size={25} style={{marginTop:10}} />
                </Right>
            </TouchableOpacity>
        );
    }
}