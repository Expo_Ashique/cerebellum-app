import React, { Component } from 'react';
import { FlatList } from 'react-native';
import NoteListItem from './NoteListItem'

export default class NotesList extends Component {
    render(){
        return(
            <FlatList
                data={ this.props.notes }
                renderItem={ ( { item } ) => <NoteListItem note = { item } onNoteItemClick = { this.props.onNoteItemClick } /> }
            />
        );
    }
}