import React, { Component } from 'react';
import { Text, View, StyleSheet } from 'react-native';
import CheckBox from 'react-native-check-box'
import Icon  from 'react-native-vector-icons/FontAwesome';
import { Row, Divider, TouchableOpacity } from '@shoutem/ui'


const styles = {
    container: {
        flexDirection: 'row'
    },

    textContainer: {
        flex: 8,
        flexDirection: 'column',
    },

    checkBoxContainer:{
        flex: 2,
        alignItems: 'center',
        justifyContent: 'center'
    },

    bookmarkCheckBox: {
        padding: 10
    },

    category: {
        fontSize: 18,
        color: 'black',
        fontWeight: 'bold',
        marginBottom: 10,
    },

    time: {
        fontSize: 14,
        marginBottom: 10
    },

    title: {
        fontSize: 20,
        color: 'black',
        textAlign:'left',
        marginBottom: 10
    }

}

export default class NoteListItem extends Component {
    render(){
        const { note } = this.props;
        return(
            <TouchableOpacity onPress = { () => this.props.onNoteItemClick()}>
                <Row>
                    <View style={ styles.textContainer }>
                        <Text style={ styles.category }> { note.category } </Text>
                        <Text style={ styles.time }> { note.time }</Text>
                        <Text style={ styles.title } noOfLines={ 2 } > { note.title } </Text>
                    </View>
                    <View style={ styles.checkBoxContainer }>
                        <CheckBox
                            style={ styles.bookmarkCheckBox }
                            onClick={ () => {}}
                            isChecked={true}
                            checkedImage={ <Icon name="bookmark" color="#5fcf80" size={ 24 }/> }
                            unCheckedImage={ <Icon name="bookmark-o" color="#5fcf80" size={ 24 }/> }
                        />
                    </View>
                </Row>
                <Divider styleName="line" />
            </TouchableOpacity>
        );
    }
}