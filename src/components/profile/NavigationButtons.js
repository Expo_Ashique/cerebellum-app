import React, { Component } from 'react';
import { View, StyleSheet, TouchableOpacity, Text } from 'react-native';
import { Button, Icon } from 'react-native-elements';


const styles = StyleSheet.create({

    container: {
        flex: 3,
        flexDirection: 'row',
        marginTop: 10,
        marginBottom: 10,
        justifyContent: 'flex-start',
        alignItems: 'center',
    },

    btnContainer : {
        flex: 1,
        paddingLeft: 10,
    },

    btn : {
        padding: 12,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: '#009688',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'transparent',
        color: '#ffffff',
        marginRight: 10,
        marginBottom: 10,
        flexDirection: 'row'
    },

    btnText:{
        color: 'white',
        fontWeight: 'bold',
        alignItems: 'center',
        flex: 4,
        textAlign: 'center'
    },

    btnIcon: {
        flex: 1
    }

});

export default class NavigationButtons extends Component {
    
    render(){
        return(
            <View style={ styles.container }>
                <View style={ styles.btnContainer }>
                    <TouchableOpacity style={ styles.btn } onPress={ () => this.props.onNavigateToPapers() }>
                            <Icon
                                name='book'
                                type='entypo'
                                size={15}
                                color='white'
                                style={ styles.btnIcon }
                            />
                        <Text style={  styles.btnText }> { 'Papers\n37'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={ styles.btnContainer }>
                    <TouchableOpacity style={ styles.btn } onPress={ () => this.props.onNavigateToFavs() }>
                            <Icon
                                name='favorite'
                                size={15}
                                color='white'
                                style={ styles.btnIcon }
                            />
                        <Text style={  styles.btnText }> { 'Favorites\n37'}</Text>
                    </TouchableOpacity>
                </View>
                <View style={ styles.btnContainer }>
                    <TouchableOpacity style={ styles.btn } onPress={ () => this.props.onNavigateToNotes() }>
                            <Icon
                                name='note-text'
                                type='material-community'
                                size={15}
                                color='white'
                                style={ styles.btnIcon }
                            />
                        <Text style={  styles.btnText }> { 'Notes\n37'}</Text>
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
}
