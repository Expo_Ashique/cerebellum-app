import React, { Component } from 'react'
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import { Icon } from 'react-native-elements'
import { TagSelect } from 'react-native-tag-select';
// import TagSelect from '../filter/TagSelect';
import PropTypes from 'prop-types'

import mainColor from './constants'

const styles = StyleSheet.create({
  tagContainer: {
    
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'flex-start'
  },
  container: {
    flexDirection: 'row',
    marginBottom: 25,
    paddingTop: 30,
    flex: 2,
    justifyContent: 'flex-start',
  },
  emailColumn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginBottom: 5,
  },
  emailIcon: {
    color: mainColor,
    fontSize: 30,
  },
  emailNameColumn: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
  },
  emailNameText: {
    color: 'gray',
    fontSize: 14,
    fontWeight: '200',
  },
  emailRow: {
    flex: 8,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  emailText: {
    fontSize: 16,
  },
  iconRow: {
    flex: 2,
    justifyContent: 'center',
  },

  badgeSelected: {
    padding: 12,
    paddingTop: 3,
    paddingBottom: 3,
    backgroundColor: '#009688',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    borderColor: 'transparent',
    color: '#ffffff',
    marginRight: 10,
    marginBottom: 10
  },
  emailIcon: {
    color: mainColor,
    fontSize: 30,
  },
})


export default class AreaOfInterest extends Component {

    constructor(props){
        super(props);
        console.log(props)
        this.state = { 
          data: props.data
        };
        
    }

    render(){
        return(
          <View style={ styles.container }>
            <View style={styles.iconRow}>
              <Icon
                  type={ this.props.iconType }
                  name={ this.props.iconName }
                  underlayColor="transparent"
                  iconStyle={styles.emailIcon}
              />
              
            </View>
            <View style={ styles.emailRow }>
              <View
                  style = { styles.tagContainer }
                  data={ this.state.data }
                  itemStyle={ styles.badgeSelected }
                  itemStyleSelected={ styles.badgeSelected }
                  ref={ (tag) => { this.tag = tag; } }>
                  {
                    this.state.data.map((i) => {
                      return(
                        <Text style = {styles.badgeSelected}>{ i.name }</Text>
                      )
                    })
                  }
              </View>
            </View>
          </View>
        );
    }
}

