import React, { Component } from 'react'
import PropTypes from 'prop-types'

import contactData from './contact.json'

import Profile from './Profile'

export default class ProfileScreen extends Component {
  render(){
    return(
      <Profile {...contactData} 
          onNavigateToPapers = { this.props.onNavigateToPapers }
          onNavigateToFavs = { this.props.onNavigateToFavs }
          onNavigateToNotes = { this.props.onNavigateToNotes }
          onNoteItemClick = { this.props.onNoteItemClick } />
    );
  }
}


ProfileScreen.navigationOptions = () => ({
  header: null,
})

ProfileScreen.propTypes = {
  navigation: PropTypes.object.isRequired,
}


