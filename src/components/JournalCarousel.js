import React, { Component } from 'react';
import { View, FlatList, ListView, StyleSheet, Dimensions, TouchableOpacity } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';
import Carousel from 'react-native-snap-carousel';
import JournalItem from './carousel/JournalItem';
import { sliderWidth, itemWidth, sliderHeight, itemHeight } from './carousel/style';
import styles from './carousel/style';


const ENTRIES1 = [
    {
        key: 1,
        uri: 'https://i.imgur.com/K3KJ3w4h.jpg',
        publishTime: '11 hrs ago',
        title: 'Neural Computing and Applications',
        subtitle: 'Follower pollingnation-feedforword neural network for load forcasting in smart distributation grid',
        author: 'Nurettin Cetinkaya, Gaddafi Sani Shehu',
        summary: 'We\'ll use the gesture responder system. The robustness is great, however I was expecting a little more information like deltas over the course of each drag update.' + 
                 'We aren\'t given that to my knowledge so we\'ll computer it ourselves.We\'ll use the gesture responder system. The robustness is great, however I was expecting a little more information like deltas over the course of each drag update.' + 
                 'We aren\'t given that to my knowledge so we\'ll computer it ourselves.'
    },
    {
        key: 2,
        uri: 'http://www.pnas.org/content/pnas/110/4/1141/F1.large.jpg',
        publishTime: '11 hrs ago',
        title: 'Latent Dirichlet Allocation',
        subtitle: 'LDA is a means of classifying objects, such as documents, based on their underlying topics.',
        author: 'Blei D, Ng A, Jordon M',
        summary: 'latent Dirichlet allocation' + 
                 '(LDA), a generative probabilistic model for collections of' + 
                 'discrete data such as text corpora. LDA is a three-level hierarchical Bayesian model, in which each' + 
                 'item of a collection is modeled as a finite mixture over an underlying set of topics. Each topic is, in' + 
                 'turn, modeled as an infinite mixture over an underlying set of topic probabilities. In the context of' + 
                 'text modeling, the topic probabilities provide an explicit representation of a document. We present' + 
                 'efficient approximate inference techniques based on variational methods and an EM algorithm for' + 
                 'empirical Bayes parameter estimation. We report results in document modeling, text classification,' + 
                 'and collaborative filtering, comparing to a mixture of unigrams model and the probabilistic LSI' + 
                 'model.'
    },
    {
        key: 3,
        uri: 'https://pbs.twimg.com/profile_images/935325968280907776/AcBo6zJc_400x400.jpg',
        publishTime: '11 hrs ago',
        title: 'MapReduce',
        subtitle: ' Simplifed Data Processing on Large Clusters',
        author: 'Jeffrey Dean, Sanjay Ghemawat',
        summary: 'MapReduce is a programming model and an assocaated implementation for processing and generating large data sets. Users specify a map function that processes a key/value pair to generate a set of intermediate key/value pairs, and a reduce function that merges all intermediate values associated with the same intermediate key. Many real world tasks are expressible in this model, as shown in the paper'
    },
    {
        key: 4,
        uri: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Sergey_Brin_cropped.jpg/220px-Sergey_Brin_cropped.jpg',
        publishTime: '11 hrs ago',
        title: 'The Anatomy of a Large-Scale HypertextualWeb Search Engine',
        subtitle: 'Simplifed Data Processing on Large Clusters',
        author: 'Sergey Brin, Lawrence Page',
        summary: 'In this paper, we present Google, a prototype of a large-scale search engine which makes heavy' + 
                 'use of the structure present in hypertext. Google is designed to crawl and index the Web efficiently' + 
                 'and produce much more satisfying search results than existing systems. The prototype with a full' + 
                 'text and hyperlink database of at least 24 million pages is available at http://google.stanford.edu/' + 
                 'To engineer a search engine is a challenging task. Search engines index tens to hundreds of' + 
                 'millions of web pages involving a comparable number of distinct terms. They answer tens of' + 
                 'millions of queries every day. Despite the importance of large-scale search engines on the web,' + 
                 'very little academic research has been done on them. Furthermore, due to rapid advance in' + 
                 'technology and web proliferation, creating a web search engine today is very different from three' + 
                 'years ago. This paper provides an in-depth description of our large-scale web search engine -- the' + 
                 'first such detailed public description we know of to date. Apart from the problems of scaling' + 
                 'traditional search techniques to data of this magnitude, there are new technical challenges involved' + 
                 'with using the additional information present in hypertext to produce better search results. This' + 
                 'paper addresses this question of how to build a practical large-scale system which can exploit the' + 
                 'additional information present in hypertext. Also we look at the problem of how to effectively deal' + 
                 'with uncontrolled hypertext collections where anyone can publish anything they want'
    },
    {
        key: 5,
        uri: 'http://www.cs.ubc.ca/~lowe/photos/david2010s.jpg',
        publishTime: '11 hrs ago',
        title: 'Distinctive Image Features from Scale-Invariant Keypoints',
        subtitle: 'Simplifed Data Processing on Large Clusters',
        author: 'David G. Lowe',
        summary: 'This paper presents a method for extracting distincti ve invariant features from images that can be used to perform reliable matching between dif ferent vie ws of an object or scene. The features are invariant to image scale and rotation, and are sho wn to pro vide rob ust matching across a a substantial range of affine dis- tortion, change in 3D vie wpoint, addition of noise, and change in illumination. The features are highly distincti ve, in the sense that a single feature can be cor - rectly matched with high probability against a lar ge database of features from man y  images. This paper also describes an approach to using these features for object recognition. The recognition proceeds by matching indi vidual fea- tures to a database of features from kno wn objects using a fast nearest-neighbor algorithm, follo wed by a Hough transform to identify clusters belonging to a sin- gle object, and finally performing verification through least-squares solution for consistent pose parameters. This approach to recognition can rob ustly identify objects among clutter and occlusion while achie ving near real-time performance.'
    },
    {
        key: 6,
        uri: 'https://carlsonschool.umn.edu/sites/carlsonschool.umn.edu/files/styles/standard_profile_pic/public/faculty/gedas_1.jpg?itok=Utwf3iiC',
        publishTime: '11 hrs ago',
        title: 'Toward the Next Generation of Recommender Systems',
        subtitle: 'A Survey of the State-of-the-Art and Possible Extensions',
        author: 'Gediminas Adomavicius, Alexander Tuzhilin',
        summary: 'This paper presents an overview of the field of recommender systems and describes the current generation ofrecommendation methods that are usually classified into the following three main categories: content-based, collaborative, and hybridrecommendation approaches. This paper also describes various limitations of current recommendation methods and discussespossible extensions that can improve recommendation capabilities and make recommender systems applicable to an even broaderrange of applications. These extensions include, among others, an improvement of understanding of users and items, incorporation ofthe contextual information into the recommendation process, support for multcriteria ratings, and a provision of more flexible and lessintrusive types of recommendations.'
    }
];

export default class JournalCarousel extends Component {

    // renderItem =  ({item, index}) => {
    //     return (
    //         <TouchableOpacity style={ {flex: 1 }} onPress={ () => this.props.onItemClick(item)}>
    //             <Card >
    //                 <CardItem>
    //                     <Left>
    //                         <Thumbnail source={{uri: item.uri}} />
    //                         <Text style={styles.defautColor}>{item.title}</Text>
    //                     </Left>
    //                 </CardItem>
    //                 <CardItem style={styles.cardItemStyle}>
    //                         <Text style={{color:"#ccc"}}>{item.publishTime}</Text>
    //                 </CardItem>
    //                 <CardItem style={styles.cardItemStyle}>
    //                     <Text style={{fontSize:22,color:'#000'}}>{item.subtitle}</Text>
    //                 </CardItem>
    //                 <CardItem style={styles.cardItemStyle}>
    //                     <Text style={{fontSize:15,fontWeight: 'bold'}}>{item.author}</Text>
    //                 </CardItem>
    //                 <CardItem style={styles.cardItemStyle}>
    //                     <Text style={{
    //                         fontSize:20,
    //                         fontWeight:'bold'
    //                     }}>Abstract</Text>
    //                 </CardItem>
    //                 <CardItem style={styles.cardItemStyle}>
    //                     <Body>
    //                         <Text style={{
    //                             textAlign: 'justify',
    //                             fontSize: 18,
    //                             padding:5,
    //                             color:'#000'
    //                         }}>
    //                             {item.summary}
    //                         </Text>
    //                     </Body>
    //                 </CardItem>
                    
    //             </Card>
    //         </TouchableOpacity>
    //     );
    // }

    _renderItemWithParallax  = ({item, index}, parallaxProps) => {
        // 
        return (
            <JournalItem
              data={item}
              even={(index + 1) % 2 === 0}
              parallax={false}
              parallaxProps={parallaxProps}
              onItemClick={ this.props.onItemClick }
            />
        );
}

    render () {

        // <FlatList
        //         data={ ENTRIES1 }
        //         renderItem={ this.renderItem }
        //         snapToInterval={ 1000 } //your element width
        //         snapToAlignment={"center"}
        //         scrollEnabled={false}
        //         onScroll={ () => console.log("hello") }
        //     />

        // <Carousel
        //           data={ENTRIES1}
        //           renderItem={ this.renderItem }
        //           sliderWidth= { Dimensions.get('window').width }
        //           itemWidth = { Dimensions.get('window').width }
        //           sliderHeight={ Dimensions.get('window').height }
        //           itemHeight={ Dimensions.get('window').height }
        //           layout='default'
        //           vertical={true}
        //           useScrollView={true}
        //     />
        // <View style={{ backgroundColor: '#ff0ff', flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}></View>


        // <Carousel
        //             style = { { backgroundColor: '#ff0ff' }}
        //             data={ENTRIES1}
        //             renderItem={this._renderItemWithParallax}
        //             sliderWidth={sliderWidth}
        //             itemWidth={itemWidth}
        //             hasParallaxImages={true}
        //             firstItem={ 0 }
        //             inactiveSlideScale={0.94}
        //             inactiveSlideOpacity={0.7}
        //             containerCustomStyle={styles.slider}
        //             contentContainerCustomStyle={styles.sliderContentContainer}
        //             loop={false}
        //             autoplay={false}
        //             onSnapToItem={(index) => console.log(index)  }
        //             isVertical = { true }
        //         />
        return (
            

            
            <Carousel
                  data={ENTRIES1}
                  renderItem={ this._renderItemWithParallax }
                  sliderWidth= { Dimensions.get('window').width }
                  itemWidth = { Dimensions.get('window').width }
                  sliderHeight={ Dimensions.get('window').height * .75 }
                  itemHeight={ Dimensions.get('window').height * .75 }
                  layout='default'
                  vertical={true}
                  useScrollView={false}
            />
            
                
            
           
        );
    }
}
