import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text , Easing,
    TouchableOpacity, ScrollView, Platform, KeyboardAvoidingView, Keyboard } from 'react-native';

import TagSelect from './TagSelect';
import { Button } from 'react-native-vector-icons/FontAwesome';
import { Icon } from 'react-native-elements';


const { width } = Dimensions.get("window");

import FlipView from 'react-native-flip-view-next';
import TagInput from 'react-native-tag-input';


export default class FilterPanel extends Component {

    constructor(props){
        super(props);
        this.state = {
            height: 0, isFlipped: false, suggestions : [], tagsSelected : [], newTags : [], newTagText: '',
            filterData: props.data 
        };
    }

    render(){
        return(
            
            <KeyboardAvoidingView style={ [ styles.frontContainer, { height: this.state.height }] }>
                    <FlipView 
                        style={ [ styles.frontContainer, { height: this.state.height }] }
                        front={this._renderFront()}
                        back={this._renderBack()}
                        isFlipped={this.state.isFlipped}
                        onFlipped={(val) => {console.log('Flipped: ' + val);}}
                        flipAxis="y"
                        flipEasing={Easing.out(Easing.ease)}
                        flipDuration={500}
                        perspective={1000}
                    />
            </KeyboardAvoidingView>
            
        )
    }
    _renderFront =() => {
        return (
            
            <View style={ [ styles.frontContainer, { height: this.state.height }] }>
                <ScrollView style={ { flex: 1 }} >
                    <TagSelect
                        data={this.state.filterData}
                        max={ 20 }
                        itemStyle={ styles.badge }
                        itemStyleSelected={ styles.badgeSelected }
                        ref={(tag) => {
                            this.tag = tag;
                        }}
                        onMaxError={() => {
                            Alert.alert('Ops', 'Max reached');
                        }}
                    />
                </ScrollView>

                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent:'center', alignItems: 'center'}} onPress={ () => this._flip() }>
                        <Icon name="plus-circle" type="feather"  size={30} color="#009688" />
                        <Text style={{ fontWeight: 'bold', color: '#009688'}}>Add New Filter</Text>

                    </TouchableOpacity>
                </View>

                
            </View>
            
        );
    }

    _renderBack = () => {

        const inputProps = {
            keyboardType: 'default',
            placeholder: 'New filters ..',
            autoFocus: false,
            style: {
              fontSize: 14,
              marginVertical: Platform.OS == 'ios' ? 10 : -2,
              color: 'white',
            },
            onSubmitEditing: this._addAndFlip
          };

        return (

            
        
            <KeyboardAvoidingView style={[ styles.backContainer, { height: this.state.height }]}>
                <TagInput
                    style={{ flex: 1}}
                    value={this.state.newTags}
                    text={ this.state.newTagText}
                    onChange={this.onChangeTags}
                    labelExtractor={this.labelExtractor}
                    onChangeText={this.onChangeText}
                    tagContainerStyle={ styles.badgeInput }
                    tagTextColor="#009688"
                    inputProps={inputProps}
                    maxHeight={75}
                />

                <KeyboardAvoidingView style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'center' }}>
                    <TouchableOpacity style={{ flexDirection: 'row', justifyContent:'center', alignItems: 'center'}} onPress={ () => this._addAndFlip() }>
                        <Icon name="done"  size={30} color="white" />
                        <Text style={{ fontWeight: 'bold', color: 'white'}}>Done</Text>
                    </TouchableOpacity>   
                </KeyboardAvoidingView>   
            </KeyboardAvoidingView>
        );
      };

      labelExtractor = (tag) => tag

      onChangeTags = (tags) => {
        this.setState({ newTags: tags });
      }
    
      onChangeText = (text) => {
        this.setState({ newTagText: text });
    
        const lastTyped = text.charAt(text.length - 1);
        const parseWhen = [',', ';', '\n'];
    
        if (parseWhen.indexOf(lastTyped) > -1) {
          this.setState({
            newTags: [...this.state.newTags, this.state.newTagText],
            newTagText: "",
          });
        }
    }

      _flip = () => {
        this.setState({isFlipped: !this.state.isFlipped});
      };
    
      _addAndFlip = () => {
          Keyboard.dismiss();
          var arr = this.state.filterData.slice() ;
          for (var i = 0 ; i < this.state.newTags.length; i++)
                arr.push({ 'id': arr.length + 1, 'label': this.state.newTags[i]});

          if (this.state.newTagText && this.state.newTagText.length > 0)
            arr.push({ 'id': arr.length + 1, 'label': this.state.newTagText });

          this.setState({
            newTags: [],
            newTagText: "",
            filterData: arr
          });

          this._flip();
      };

    itemsSelected(){
        return this.tag.itemsSelected;
    }

    expand(){
        this.expanded = true;
        this.setState({ height: 600 });
    }

    collapse(){
        this.expanded = false;
        this.setState({ height: 0 });
    }

    toggle(){
        if (this.expanded)
            this.collapse();
        else
            this.expand();
    }

    isExpanded(){
        return this.expanded;
    }
  }

  


  const styles = StyleSheet.create({
   
    
    frontContainer: {
      width: width,
      backgroundColor: "white",
      zIndex: 10000
    },

    backContainer: {
        width: width,
        backgroundColor: "#009688",
        zIndex: 10000
      },

    badge: {
        padding: 12,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: 'transparent',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: '#009688'
    },

    badgeSelected: {
        padding: 12,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: '#009688',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'transparent'
    },

    badgeInput: {
        padding: 12,
        paddingTop: 3,
        paddingBottom: 3,
        backgroundColor: 'white',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
        borderColor: 'transparent',
        marginLeft: 10
    },




  });

  