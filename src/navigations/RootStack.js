/**
 * The Root Stack -> First Home Screen (With tabs) then if item is clicked go to details screen
 * If back is pressed in details screen, should go back to home tab
 * 
 * Author: SayeedM
 */

import React, { Component } from 'react';
import JournalContainer from '../containers/JournalContainer';
import DetailsScreen from '../screens/DetailsScreen';
import { StackNavigator } from 'react-navigation';
import HomeTab from './HomeTab';
import NotesContainer from '../containers/NotesContainer';
import SearchResultListContainer from '../containers/SearchResultContainer';
import NoteDetailContainer from '../containers/NoteDetailContainer';


const RootStack = StackNavigator({
    Home: { 
        screen: HomeTab,
        navigationOptions:{
            header: null
        } 
    },
    Details: { 
        screen: DetailsScreen,
        navigationOptions:{
            header: null
        } 
    },
    Notes: {
        screen: NotesContainer,
        navigationOptions: {
            header: null
        }
    }, 
    Journals: {
        screen: JournalContainer,
        navigationOptions: {
            header: null
        }
    },
    SearchResult:{
        screen: SearchResultListContainer,
        navigationOptions:{
            header: null
        }
    },
    NoteDetail:{
        screen: NoteDetailContainer,
        navigationOptions:{
            header: null
        }
    } 
});

export default class RootComponent extends Component {

    render(){
        return <RootStack />
    }

}
