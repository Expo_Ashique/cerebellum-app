import React, { Component } from 'react';
import { TabNavigator } from 'react-navigation';
import HomeScreen from '../screens/HomeScreen';

const JournalTabNavigation = TabNavigator(
    {
        Home1: {
            screen: HomeScreen
        },
        Home2: {
            screen: HomeScreen
        }
    }
);

export default JournalTabNavigation;