import React, { Component } from 'react';
import { TabNavigator, StackNavigator, TabBarBottom } from 'react-navigation';
import JournalContainer from '../containers/JournalContainer';
import SettingsScreen from '../screens/SettingsScreen';
// import ProfileScreen from '../screens/ProfileScreen';
import DetailsScreen from '../screens/DetailsScreen';
import JournalsScreen from '../screens/JournalsScreen';
import MyJournalScreen from '../screens/MyJournalScreen';
import SearchScreen from '../screens/SearchScreen';
import  Ionicons from 'react-native-vector-icons/Ionicons';
import ProfileScreen from '../components/profile/index'
import { Icon } from 'react-native-elements';

const SettingsStack = StackNavigator(
    {
        Profile: { screen: ProfileScreen },
        Settings: { screen: SettingsScreen }
    }
);

const JournalStack = TabNavigator({
    "My Journals": { screen: MyJournalScreen },
    "All Journals": { screen: JournalsScreen },

});



export default class HomeTab extends Component {

    onNavigateToPapers = () => {
        this.props.navigation.navigate('Journals');
    }
    
    onNavigateToFavs = () => {
        this.props.navigation.navigate('Journals');
    }

    onNavigateToNotes = () => {
        this.props.navigation.navigate('Notes', {
            'onNoteItemClick': this.onNoteItemClick
        });
    }

    onNoteItemClick = () => {
        this.props.navigation.navigate('NoteDetail');
    }

    onSearchSubmit = (query) => {
        this.props.navigation.navigate('SearchResult', { 'query': query });
    }

    onJournalItemClick = (item) => {
        this.props.navigation.navigate('Details', {'data': item});
    }



    render(){
        
        const HomeTabNavigator = TabNavigator(
            {
                Home: { 
                    screen: props => <JournalContainer onJournalItemClick = { this.onJournalItemClick }/>
                },
                Journal: { screen: JournalStack },
                Search: { 
                    screen: props => <SearchScreen onSearchSubmit={ this.onSearchSubmit } /> 
                },
                User: { 
                    screen: props => {
                        return( 
                            <ProfileScreen 
                                onNavigateToPapers = { this.onNavigateToPapers }
                                onNavigateToFavs = { this.onNavigateToFavs }
                                onNavigateToNotes = { this.onNavigateToNotes }
                                onNoteItemClick = { this.onNoteItemClick }
                            />
                        );
                    },
                }
            },
            {
                backBehavior: 'none',
                navigationOptions: ({ navigation }) => ({
                    tabBarIcon: ({ focused, tintColor }) => {
                        const { routeName } = navigation.state;
                        let iconName;
                        if (routeName === 'Home') {
                            iconName = `ios-home${focused ? '' : '-outline'}`;
                        } else if (routeName === 'Journal') {
                            iconName = `ios-list-box${focused ? '' : '-outline'}`;
                        }
                        else if (routeName === 'Search') {
                            iconName = `ios-search${focused ? '' : '-outline'}`;
                        }
                        else if (routeName === 'User') {
                            return <Icon name="account-circle" size={25} color={tintColor} />
                        }
        
                        // You can return any component that you like here! We usually use an
                        // icon component from react-native-vector-icons
                        return <Ionicons name={iconName} size={25} color={tintColor} />;
                    },
                }),
                tabBarComponent: TabBarBottom,
                tabBarPosition: 'bottom',
                tabBarOptions: {
                    activeTintColor: '#01C89E',
                    inactiveTintColor: 'gray',
                },
                animationEnabled: false,
                swipeEnabled: false,
            }
        );

        return(
            <HomeTabNavigator onNavigateToNotes = {this.props.onNavigateToNotes } />
        )

    }
}