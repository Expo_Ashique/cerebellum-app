import React,{Component} from 'react';
import {View,Text} from 'react-native';

class ProfileScreen extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <View>
                <Text>This is a My profile Screen</Text>
            </View>
        )
    }
}


// Make the component available to other parts of the app
export default ProfileScreen;