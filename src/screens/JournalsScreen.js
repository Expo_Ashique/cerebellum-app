import React,{Component} from "react";
import {View,Text} from 'react-native';

class JournalsScreen extends Component {
    render() {
        return (
            <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                <Text>This is a journals Screen</Text>
            </View>
        );
    }
}

export default JournalsScreen;
