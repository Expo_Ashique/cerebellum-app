import React,{Component} from "react";
import {Text,View, TouchableOpacity} from 'react-native';
import { Container, Content, Card, CardItem, Thumbnail, Left, Body} from 'native-base';
import Time from "../components/Time";
import { Icon } from 'react-native-elements';

class DetailsScreen extends Component {

    constructor(props){
        super(props);
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <Content>
                    <Card style={{flex: 0}}>
                        <CardItem>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                                    <Text style={ { flex: 6 } }>Neural Computing and Applications</Text>
                                    <TouchableOpacity style={{ flex: 2, alignItems: 'flex-end'}}>
                                        <Icon name="share-variant" type="material-community" size={30} color="#5fcf80" />
                                    </TouchableOpacity>
                                </View>
                        </CardItem>
                        <CardItem style={styles.cardItemStyle}>
                                <Text style={{color:"#ccc"}}>11 hrs ago</Text>
                        </CardItem>
                        <CardItem style={styles.cardItemStyle}>
                            <Text style={{fontSize:22,color:'#000'}}>Follower pollingnation-feedforword neural network for load forcasting in smart distributation grid</Text>
                        </CardItem>
                        <CardItem style={styles.cardItemStyle}>
                            <Text style={{fontSize:15,fontWeight: 'bold'}}>Nurettin Cetinkaya, Gaddafi Sani Shehu</Text>
                        </CardItem>
                        <CardItem style={styles.cardItemStyle}>
                            <Text style={{
                                fontSize:20,
                                fontWeight:'bold'
                            }}>Abstract</Text>
                        </CardItem>
                        <CardItem style={styles.cardItemStyle}>
                            <Body>
                                <Text style={{
                                    textAlign: 'justify',
                                    fontSize: 18,
                                    padding:5,
                                    color:'#000'
                                }}>
                                    We'll use the gesture responder system. The robustness is great, however I was expecting a little more information like deltas over the course of each drag update.
                                    We aren't given that to my knowledge so we'll computer it ourselves.We'll use the gesture responder system. The robustness is great, however I was expecting a little more information like deltas over the course of each drag update.
                                    We aren't given that to my knowledge so we'll computer it ourselves.
                                </Text>
                            </Body>
                        </CardItem>

                        
                    </Card>



                </Content>

                <TouchableOpacity
                        style={{
                            borderWidth:1,
                            borderColor:'transparent',
                            alignItems:'center',
                            justifyContent:'center',
                            width:70,
                            position: 'absolute',                                          
                            bottom: 10,                                                    
                            right: 10,
                            height:70,
                            backgroundColor:'#5fcf80',
                            borderRadius:100,
                            zIndex: 1000000
                        }}>
                    <Icon name="bookmark" color="white" size={ 24 }/>
                </TouchableOpacity>


                <TouchableOpacity
                        style={{
                            borderWidth:1,
                            borderColor:'transparent',
                            alignItems:'center',
                            justifyContent:'center',
                            width:70,
                            position: 'absolute',                                          
                            bottom: 10,                                                    
                            right: 100,
                            height:70,
                            backgroundColor:'#5fcf80',
                            borderRadius:100,
                            zIndex: 1000000
                        }}>
                    <Icon name="file-pdf-box" color="white" size={ 24 } type="material-community"/>
                </TouchableOpacity>

                
                
            </View>
        );
    }
}

const styles={
    cardItemStyle:{
        marginTop:0,
        paddingTop:0
    },
    defautColor:{
        color:'#000'
    }
}

export default DetailsScreen;