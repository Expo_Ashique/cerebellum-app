import React,{Component} from 'react';
import { Image } from 'react-native';
import { Container, Header, Content, Card, CardItem, Thumbnail, Text, Button, Icon, Left, Body } from 'native-base';

class MyJournalScreen extends Component{

    constructor(props){
        super(props);
    }

    render(){
        return (
            <Container>
                <Content style={{flex:1,flexDirection:'column'}}>
                    <Card style={{flex: 0}}>
                        <CardItem>
                            <Body>
                            <Image source={{uri: 'https://i.imgur.com/K3KJ3w4h.jpg'}} style={{height: 200, width: 200, flex: 1}}/>
                            <Text>
                                Expert System with Applications
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Button transparent>
                                    <Text>Unfollow</Text>
                                </Button>
                            </Left>
                        </CardItem>
                    </Card>
                    <Card style={{flex: 0}}>
                        <CardItem>
                            <Body>
                            <Image source={{uri: 'https://i.imgur.com/K3KJ3w4h.jpg'}} style={{height: 200, width: 200, flex: 1}}/>
                            <Text>
                                Expert System with Applications
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Button transparent>
                                    <Text>Unfollow</Text>
                                </Button>
                            </Left>
                        </CardItem>
                    </Card>
                    <Card style={{flex: 0}}>
                        <CardItem>
                            <Body>
                            <Image source={{uri: 'https://i.imgur.com/K3KJ3w4h.jpg'}} style={{height: 200, width: 200, flex: 1}}/>
                            <Text>
                                Expert System with Applications
                            </Text>
                            </Body>
                        </CardItem>
                        <CardItem>
                            <Left>
                                <Button transparent>
                                    <Text>Unfollow</Text>
                                </Button>
                            </Left>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        )
    }
}


// Make the component available to other parts of the app
export default MyJournalScreen;