import React,{Component} from "react";
import {Text,Alert,ActivityIndicator,View,TouchableOpacity} from "react-native";
import {Container, Header,Body, Title, Content,Footer, FooterTab, Button, Icon,List,ListItem } from 'native-base'
import {getAllResearch} from "../services/research";
import ListDataItem from "../components/ListItem";

class HomeScreen extends Component {
    constructor(props){
        super(props);
        this.state={
            isLoading:true,
            data:[],
            isError:false
        };
        console.disableYellowBox = true;
        console.log( this.props.navigation.navigate('Settings'));
    }

    componentDidMount(){

        getAllResearch().then((responseJson)=>{
            this.setState({
                isLoading:false,
                data:responseJson
            });
        },error=>{
            Alert.alert("Error", "Something happend, please try again")
        })
    }

    render() {
        const view = this.state.isLoading ?(
            <View style={{flex:1,alignItems:'center',justifyContent:'center'}}>
                <ActivityIndicator animating={this.state.isLoading} color="#00f0ff"/>
                <Text style={{marginTop:8}} children="Please Wait..."/>
            </View>
        ):(
            <List
                dataArray = {this.state.data}
                renderRow={(item)=>{
                    return (
                        <ListItem>
                            <ListDataItem data={item} navigation = {this.props.navigation}></ListDataItem>
                        </ListItem>
                    )
                }}
            />
        );
        return (
            <Container>
                {/*<Header style={{backgroundColor:'#fff'}}>
                    <Body>
                    <Title style={{color:'#000'}}>All</Title>
                    </Body>
                </Header>*/}
                <Content
                    contentContainerStyle={{backgroundColor:'#fff'}}
                    padder={false}
                    children={view}
                />
            </Container>
        );
    }
}

export default HomeScreen;