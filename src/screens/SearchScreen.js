import React,{Component} from "react";
import { View, TextInput } from 'react-native';
import  Ionicons from 'react-native-vector-icons/Ionicons';

const styles = {
    window:{
        flex: 7,
        alignItems: 'center',
        backgroundColor: 'white'

    },

    searchSection: {
        flex: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'flex-end',
        backgroundColor: '#fff',
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        width: '70%'
        
    },
    searchIcon: {
        padding: 10,
    },
    input: {
        flex: 1,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 10,
        paddingLeft: 0,
        backgroundColor: '#fff',
        color: '#424242',
    },
    empty:{
        flex: 4
    }
}

export default class SearchScreen extends Component {
    render() {
        return (
                <View style={ styles.window }>
                    <View style={ styles.searchSection }>
                        <Ionicons style={styles.searchIcon} name="ios-search" size={20} color="#000"/>
                        <TextInput
                            style={styles.input}
                            placeholder="Search.."
                            onChangeText={(searchString) => {this.setState({searchString})}}
                            underlineColorAndroid="transparent"
                            returnKeyType='Search'
                            onSubmitEditing={ () => this.props.onSearchSubmit(this.state.searchString) }
                        />
                    </View>
                    <View style={ styles.empty }></View>
                </View>
            
        );
    }
}
