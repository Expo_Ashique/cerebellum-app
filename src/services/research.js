import {orderBy} from 'lodash';
import {_api_key,lang,category,articles_url} from "../config/rest_config";


export async function getAllResearch() {
    try{
        let articles = await fetch(`${articles_url}?category=${category}&country=${lang}`,{
            headers:{
                'X-API-KEY':_api_key
            }
        }).then((response) => response.json())
        return orderBy(articles.articles,'publishedAt','desc');
    }catch (error){
        throw error
    }
}