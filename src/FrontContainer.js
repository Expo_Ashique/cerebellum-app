import React, { Component } from 'react';
import { View, StyleSheet, Dimensions, Text } from 'react-native';
import { Header, FormLabel, FormInput, Icon } from "react-native-elements";


const { width } = Dimensions.get("window");


export default class FrontContainer extends Component {

    constructor(){
        super();
        this.state = {height: 0 };
    }

    render(){
        return (
            <View style={ [ styles.frontContainer, { height: this.state.height }] }>
                <FormLabel>Name</FormLabel>
                <FormInput onChangeText={() => console.log("Working")} />
                <FormLabel>Password</FormLabel>
                <FormInput onChangeText={() => console.log("Working")} />
            </View>
        );
    }

    expand(){
        this.expanded = true;
        this.setState({ height: 600 });
    }

    collapse(){
        this.expanded = false;
        this.setState({ height: 0 });
    }

    toggle(){
        if (this.expanded)
            this.collapse();
        else
            this.expand();
    }
  }

  


  const styles = StyleSheet.create({
   
    
    frontContainer: {
      width: width,
      backgroundColor: "papayawhip"
    },

    handler: {
      height: 100,
      marginTop: 100,
      width: width,
      alignItems: "center"
    }
  });