import React, { Component } from 'react';
import { View } from 'react-native';
import NoteList from '../components/notes/NotesList';
import Handler from '../Handler';
import FilterPanel from '../components/filter/FilterPanel';

const notes = [
    {
        key: '1',
        category: 'Image Processing',
        time: '8 min ago',
        title: 'Removing ISO generated noise from digital images'
    },
    {
        key: '2',
        category: 'Big Data',
        time: '8 min ago',
        title: 'Streaming Analytics on Apache Spark'
    },
    {
        key: '3',
        category: 'AI',
        time: '8 min ago',
        title: 'Generating similiar images using GAN'
    },
    {
        key: '4',
        category: 'AI, Games',
        time: '8 min ago',
        title: 'Generating Level Maps for 2D arcade games using RNN'
    },
    {
        key: '5',
        category: 'AI, Math',
        time: '8 min ago',
        title: 'Understanding Sigmoid Function'
    },
    {
        key: '6',
        category: 'Image Processing',
        time: '8 min ago',
        title: 'Retouching Signature Images'
    },
];

const filterData = [
    { id: 1, label: 'Image'},
    { id: 2, label: 'Neural Network'},
    { id: 3, label: 'CNN'},
    { id: 4, label: 'RNN'},
    { id: 5, label: 'HMM'}
];

export default class NotesContainer extends Component {
    
    onButtonClick = () => {
        this.container.toggle();
        if (!this.container.isExpanded()){
            let items = this.container.itemsSelected();
            if (items.length == 0)
                items[0] = {id: 1, label: 'All'};
            
            this.list.setData(items);

        }
    }

    render(){
        
        console.log(this.props);

        return(
            <View>
                <Handler ref={ref => this.list = ref } onButtonClick={ this.onButtonClick} data={[{ id: 1, label: 'All'}]} />
                <FilterPanel data={ filterData } ref={ ref  => this.container = ref } />
                <NoteList notes={ notes } onNoteItemClick = { this.props.navigation.state.params.onNoteItemClick } />
            </View>
        );
    }
}