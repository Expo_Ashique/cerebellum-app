import React, { Component } from 'react';
import { View, Text } from 'react-native';
import SearchResultList from '../components/search/SearchResultList';
import { Divider } from '@shoutem/ui';


const articles = [
    {
        key: '1',
        category: 'Image Processing',
        time: '8 min ago',
        title: 'Removing ISO generated noise from digital images'
    },
    {
        key: '2',
        category: 'Big Data',
        time: '8 min ago',
        title: 'Streaming Analytics on Apache Spark'
    },
    {
        key: '3',
        category: 'AI',
        time: '8 min ago',
        title: 'Generating similiar images using GAN'
    },
    {
        key: '4',
        category: 'AI, Games',
        time: '8 min ago',
        title: 'Generating Level Maps for 2D arcade games using RNN'
    },
    {
        key: '5',
        category: 'AI, Math',
        time: '8 min ago',
        title: 'Understanding Sigmoid Function'
    },
    {
        key: '6',
        category: 'Image Processing',
        time: '8 min ago',
        title: 'Retouching Signature Images'
    },
];


export default class SearchResultListContainer extends Component {
    render(){
        return(
            <View>
                <View style={ { alignItems: 'center', padding: 20 } }>
                    <Text style={{ fontSize: 20, color: 'black', marginBottom: 20}}>Query</Text>
                    <Divider styleName="line" />
                </View>
                <SearchResultList articles={ articles } />
            </View>
        );
    }
}