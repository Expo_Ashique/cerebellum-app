import React, { Component } from 'react';
import { Dimensions, StyleSheet, View, Text } from 'react-native';

import { Header, FormLabel, FormInput, Icon } from "react-native-elements";
import JournalTopTabBar from '../components/tabs/JournalTopTabBar';
import JournalCarousel from '../components/JournalCarousel';
import Handler from '../Handler';
import FilterPanel from '../components/filter/FilterPanel';




const { width } = Dimensions.get("window");

const MAXIMUM_HEIGHT = 250;
const HANDLER_HEIGHT = 30;
const OFFSET_TOP = 69;



const filterData = [
    { id: 1, label: 'Image'},
    { id: 2, label: 'Neural Network'},
    { id: 3, label: 'CNN'},
    { id: 4, label: 'RNN'},
    { id: 5, label: 'HMM'}
];



export default  class JournalContainer extends Component {

    onItemClick = (item) => {
        if (this.props.onJournalItemClick)
            this.props.onJournalItemClick(item);
        else
            this.props.navigation.navigate('Details', { 'data' : item});
    }


    onButtonClick = () => {
        this.container.toggle();
        if (!this.container.isExpanded()){
            let items = this.container.itemsSelected();
            if (items.length == 0)
                items[0] = {id: 1, label: 'All'};
            
            this.list.setData(items);

        }
    }

    render() {
        return (
            
            <View>  
                <Handler ref={ref => this.list = ref } onButtonClick={ this.onButtonClick} data={[{ id: 1, label: 'All'}]} />
                <FilterPanel data={ filterData } ref={ ref  => this.container = ref } />
                <JournalCarousel onItemClick={ this.onItemClick } />
            </View>
        );
    }

    
      
      
      
      
}
