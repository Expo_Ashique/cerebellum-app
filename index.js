import { AppRegistry } from 'react-native';
import RootComponent from './src/navigations/RootStack';

AppRegistry.registerComponent('Research', () => RootComponent);
